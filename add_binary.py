def add_binary(a,b):
    return bin(a+b).replace("0b", "")


# formating with binary "b"
def add_binary(a, b):
    return format(a + b, 'b')


print(add_binary(5,9))