def fizzbuzz(number):
    if number % 3 == 0 and number % 5 == 0 and number > 0:
        return "fizzbuz"
    elif number % 3 == 0 and number > 0:
        return "fizz"
    elif number % 5 == 0 and number > 0:
        return "buzz"
    else:
        return number
    
print(fizzbuzz(0))
    