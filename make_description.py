"""def make_description(name, attributes):
    list = [(name)]
    for k,v in attributes.items():     
        list.append(v)
        list.append(k)
            
    return list"""

def make_description(name, attributes):
    pieces = [name]
    for k, v in attributes.items():
        pieces.append(f"{v} {k}")
    return ", ".join(pieces)



print(make_description("Lulu", {"hair": "red", "eyes": "blue"}))