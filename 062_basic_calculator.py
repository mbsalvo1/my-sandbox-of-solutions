def basic_calculator(left, op, right):    
    if op == "+":                        
        return left + right              
    if op == "-":                         
        return left - right                
    if op == "*":                        
        return left * right                
    if op == "/":                          
        return left / right
    
print(basic_calculator(10, "+", 12))
