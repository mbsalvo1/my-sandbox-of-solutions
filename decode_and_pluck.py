import json
def decode_and_pluck(input_json, fields):
    info = json.loads(input_json)
    field = {}
    for i in fields:
        field[i] = info[i]
    return field
print(decode_and_pluck('{"a": 1, "b": 2, "c": 3}', ["a", "c"]))