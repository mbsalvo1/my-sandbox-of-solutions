# range a -z
# if upper return upper + 13
# if i.isupper() and i >= a and i <= z:

# if lower return lower + 13
# if i.islower() and i >= a and i <= z:

# if letter not a-z return as shown
# if != i.isalpha

def rot13(message):
    letts = ""
    for i in message:
        if i >= "a" and i <= "m":
            i = ord(i) + 13
            letts += chr(i)
        elif i >= "A" and i <= "M":
            i = ord(i) + 13
            letts += chr(i)
        elif i >= "n" and i <= "z":
            i = ord(i) - 13
            letts += chr(i)
        elif i >= "N" and i <= "Z":
            i = ord(i) - 13
            letts += chr(i)
        else:
            letts += i
    return letts

print(rot13("test"))
print(rot13("Test1@"))
