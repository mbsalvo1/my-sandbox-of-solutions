def shift_letters(word):                       
    new_word = ""                             
    for letter in word:                        
        if letter == "Z":                       
            new_letter = "A"                    
        elif letter == "z":                    
            new_letter = "a"                    
        else:                                   
            new_letter = chr(ord(letter) + 1)   
        new_word += new_letter                 
    return new_word                             

print (shift_letters("import"))