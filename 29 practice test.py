def mystery(request):
    if request.method == "POST":
        form = WidgetForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]

            widget = Widget.objects.create(name=name)

            return redirect(widget)
    else:
        form = WidgetForm()

    context = {
        "form": form
    }

    return render(request, "template.html", context)