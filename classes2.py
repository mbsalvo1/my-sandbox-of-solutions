"""
class: InvoiceItem
​
required state:
- price    : unit price of the item
- quantity : the number or amount of the item purchased
​
methods:
- get_total() : return the line total (price * quantity)
​
example:
item = InvoiceItem(2.50, 3)
print(item.get_total())     # 7.5
​
"""
​
class InvoiceItem:
    def __init__(self, price, quantity):
        self.price = price
        self.quantity = quantity       
​
    def get_total(self):
        return self.price * self.quantity
        
​
# item = InvoiceItem(2.50, 3)
# print(item)
# print(item.get_total())     # 7.5
​
"""
class Invoice
​
required state:
- tax_rate       : the tax-rate for this invoice
​
methods:
- add_item(item) : add an InvoiceItem to this invoice
- get_subtotal() : return the subtotal for all of the items
- get_tax()      : return the tax as a percentage of the subtotal
- get_total()    : return the sum of the subtotal and the taxes
​
example:
invoice = Invoice(0.1)
invoice.add_item(InvoiceItem(1.1, 2))
invoice.add_item(InvoiceItem(5.0, 1))
​
print(invoice.get_subtotal()) # 7.2
print(invoice.get_tax())      # 0.72
print(invoice.get_total())    # 7.92
​
"""
​
# TODO: implement this class
class Invoice:
​
    def __init__(self, tax_rate):
        self.tax_rate = tax_rate
        self.items = []
​
    def add_item(self, item):
        # We need to store the item on this invoice
        self.items.append(item)
​
    def get_subtotal(self):
        subtotal = 0
        for item in self.items:
            subtotal += item.get_total()
        return subtotal
​
    def get_tax(self):
        subtotal = self.get_subtotal()
        tax = subtotal * self.tax_rate
        return tax
​
    def get_total(self):
        subtotal = self.get_subtotal()
        tax = self.get_tax()
        return subtotal + tax
​
    
​
invoice = Invoice(0.1)
item1 = InvoiceItem(1.1, 2)
invoice.add_item(item1)
invoice.add_item(InvoiceItem(5.0, 1))
​
#print(my_invoice.items)
​
print(invoice.get_subtotal()) # 7.2
print(invoice.get_tax())      # 0.72
print(invoice.get_total())    # 7.92