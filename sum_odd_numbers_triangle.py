def row_sum_odd_numbers(n):
    beg_row = n * (n-1) + 1
    total = 0
    for i in range(n):
        total += beg_row
        beg_row +=2
    return total

print(row_sum_odd_numbers(5))