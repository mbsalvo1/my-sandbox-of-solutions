def find_shipment_weight(shipment):
    total = 0
    for i in shipment:
        pounds = i["product_weight_pounds"]
        quantity = i["quantity"]
        total += pounds * quantity
        
    return total

print(find_shipment_weight([
    {
        "product_name": "beans",
        "product_weight_pounds": 2,
        "quantity": 5,
    },
    {
        "product_name": "rice",
        "product_weight_pounds": 1.5,
        "quantity": 7,
    },
]))