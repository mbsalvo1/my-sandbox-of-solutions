def shift_cipher(message, shift):
    """message = ord(message) + shift
    return chr(message)"""
    cipher = ""
    for i in message:
        cipher += chr(ord(i) + shift)
    return cipher
# easier to read
#    for i in message:
#        message = ord(i) + shift
#        cipher += chr(message)
#    return cipher

print(shift_cipher("b", 1))
print(shift_cipher("b", 2))
print(shift_cipher("b", -1))
print(shift_cipher("bbc", 1))
print(shift_cipher("bbc", -1))
print(shift_cipher("bbc", 3))
