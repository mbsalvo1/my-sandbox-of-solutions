def reverse_entries(dictionary):
    new={}
    for k,v in dictionary.items():     
        new[v] = k
            
    return new
   
print(reverse_entries({"one": 1, "two": 2}))