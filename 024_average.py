"""def calculate_average(values):
    sum = 0
    
    for num in values:
        sum = sum + num
        average = sum / len(values)
    if len(values) == 0:
        return None
    
    else:
        return average

print(calculate_average([1, 2, 3, 7]))"""

def calculate_average(values):
    sum = 0
    
    if len(values) == 0:
        return None
    
    for num in values:        
        sum = sum + num
    average = sum / len(values)
    
    return average

print(calculate_average([9,9,9,9,5]))

"""def calculate_average(values):
    sum = 0
    for num in values:
        sum = sum + num
    average = sum / len(values)
    if len(values) == 0:
        return None
    else:
        return average

print(calculate_average([5, 4, 3]))"""
