import math                                   
                                                 
                                                
def safe_divide(numerator, denominator):        
    if denominator == 0:                     
        return math.inf                         
    return numerator / denominator

print(safe_divide(2, 10))
