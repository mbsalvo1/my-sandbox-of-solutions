def remove_duplicates(values):  
    output = []                  
    for value in values:            
        if value not in output:     
            output.append(value)    
    return output 

print(remove_duplicates([1, 11, 1, 11, 12, 12, 2]))