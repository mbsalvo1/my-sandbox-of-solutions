def can_skydive(age, has_form):
    if age >= 18 and has_form == True:
        return "You can skydive"
    else:
        return "You need to grow up"

print(can_skydive(20, False))