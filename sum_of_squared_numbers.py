def sum_of_squares(values):
    total = 0
    if len(values) == 0:
        return "None"
    for i in values:
        total = total + i * i
    return total
    
print(sum_of_squares([]))
print(sum_of_squares([1,2,3]))
print(sum_of_squares([-1, 0, 1]))
    