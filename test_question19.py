def add(items):
    total = 0
    useless = ""
    for item in items:
        if item == str(item):
            useless += item
        elif item == int(item) or item == float(item):
            total += item
    return total

print(add([1,2,"a", 4, "b", 6]))