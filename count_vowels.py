def count_vowels(word):
    number_of_vowels = 0
    for letter in word:
        if letter in "aeiou": 
            number_of_vowels += 1
    return number_of_vowels
print(count_vowels("hello"))