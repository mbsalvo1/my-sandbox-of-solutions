# classes or (instances) terms in oop (object oriented programming)
# model the real world with classes and objects
# imperative programming - wiring code that runs one line at a time
# function programming

# class - a description of a "kind" of thing. A blueprint. A Template.
# An idea of a thing
# We define two tpes of things with classes
# state (data) (attributes) (properties)
# behavior (functions) - (method)


# naming conventions for classes
# capitalized CamelCase
# They should be singular (mostly)

# indent (block) with class

# a blueprint for making rabbits
class Rabbit:
    # static state
    ears = 2
    # dynamic state
    # the initializer (constructor)
    def__init__(self, name):
        pass
    # behavior - mehtod always takes self as first parameter
    def hop(self):
        # what is (self) it is the current instance
        print("hop hop hop")


# make actual rabbit (instances of the Rabbit class)
thumper = Rabbit("Thumper")
flopsy = Rabbit("Flopsy")

rabbit.hop()
thumper.hop()
flopsy.hop()

print(rabbit.ears)
print(thumper.ears)
print(flopsy.ears)