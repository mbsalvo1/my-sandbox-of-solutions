class ReceiptItem:
    def __init__(self, quantity, price):
        self.quantity = quantity
        self.price = price
        
    def get_total(self):
        return self.quantity * self.price
drinks = ReceiptItem(2, 3.99)
print(drinks.get_total())