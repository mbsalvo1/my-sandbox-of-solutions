def max(*values):
    max_value = 0
    for value in values:
        if value >= max_value:
            max_value = value
    return max_value

print(max(1, 1, 2))
print(max(1, 2, 3))