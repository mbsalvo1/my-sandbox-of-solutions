class Person:
    def __init__(self, name):
        self.name = name


def names_in_list(names, people):
    bools = []
    for name in names:
        found = False
        for person in people:
            if name == person.name:
                found = True
        bools.append(found)
    return bools


list_of_people = [
    Person('Laila'),
    Person('Evander'),
    Person('Talia'),
    Person('Asha'),
]

result = names_in_list(['Asha', 'Azami', 'Evander'], list_of_people)
print(result)