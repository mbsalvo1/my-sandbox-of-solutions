
class Student:                                          
    def __init__(self, name):                              
        self.name = name                                   
        self.scores = []                                  

    def add_score(self, score):                             
        self.scores.append(score)                          

    def get_average(self):                                
        if len(self.scores) == 0:                           
            return None                                     
        return sum(self.scores) / len(self.scores)

print(student.get_average())    
student.add_score(80)
print(student.get_average())    
student.add_score(90)
student.add_score(82)
print(student.get_average())    

