# return second greatest
def great(numbers):
    if len(numbers) <=1:
        return "None"
    return sorted(numbers)[-2]
print(great([1,10,90,80, 80, 90]))
print(great([1]))
print(great([]))