def well(x):
    ideas = 0
    for i in x:
        if i == "good":
            ideas += 1
    if ideas > 0 and ideas <= 2:
        return "publish"
    if ideas > 2:
        return "I smell a series!"
    if ideas == 0:
        return "Fail!"

"""def well(x):
    c = x.count('good')
    return 'I smell a series!' if c > 2 else 'Publish!' if c else 'Fail!' """
print(well(["good", "good", "good"]))
print(well(["good"]))
print(well(["good", "bad"]))
print(well(["bad", "bad"]))
    