# without looking back

class Employee:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
    def get_fullname(self):
        return self.first_name + " " + self.last_name
    def get_email(self):
        return self.first_name.lower() + "." + self.last_name.lower() + "@company.com"
employee = Employee("Duska", "Ruzicka")

print(employee.get_fullname())
print(employee.get_email())