def total_revenue(product_sales):
    total = 0
    for i in product_sales:
        quantity = i["sales"]
        cost = i["price"]
        total += cost * quantity       
    return total

product_sales = [
    { "sales": 11, "price": 9,   },
    { "sales": 5,  "price": 1.5, },
]

print(total_revenue(product_sales))
