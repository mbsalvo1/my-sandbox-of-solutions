"""def remove_every_other(my_list):
    result = []
    long = len(my_list)
    for i in my_list[0:long:2]:
        result.append(i)
    return result"""

def remove_every_other(my_list):
    return my_list[0:len(my_list):2]

print(remove_every_other(["Keep", "Remove", "Keep", "Remove", "Keep"]))
