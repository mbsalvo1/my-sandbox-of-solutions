# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.

def pairwise_add(list1, list2):
    list3 = []
    for i in zip(list1, list2):
        list3.append(i)
    return list3

# list 3 = list of sums for every i in zip(list1, list2)
# list 3 is wrapping that statement
"""def pairwise_add(list1, list2):
    list3 = [sum(i) for i in zip(list1, list2)]
    return list3"""
        
print(pairwise_add([1, 2, 3, 4], [4, 5, 6, 7]))
print(pairwise_add([100, 200, 300], [10, 1, 180]))