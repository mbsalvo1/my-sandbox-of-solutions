# this is an example of random outpyt divisible by 5 and 7 hence % 35

import random                               
def specific_random():                      
    good_numbers = []                       
    for i in range(1, 500):                 
        if i % 35 == 0:                     
            good_numbers.append(i)          
    return random.choice(good_numbers)

print(specific_random())