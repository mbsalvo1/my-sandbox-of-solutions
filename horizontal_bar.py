# returns a list of letters in alpha order
def horizontal_bar_chart(sentence):
    bag = {}
    s = sorted(sentence)
    for let in sentence:
        print(type(let))
        if let >= "a" and let <= "z":
            if let in bag:
                bag[let] = bag[let] + let
            else:
                bag[let] = let
    return sorted(bag.values())

print(horizontal_bar_chart("abba has a banana"))

def horbar(sentence):
    s = sorted(sentence)
    letters = ""
    for i in s:
        if i.isalpha():
            letters += i
    return [letters]

print(horbar("abba has a banana"))
    
    
    
        
        

