# Classes (Instances) terms in OOP (Object Oriented Programming)
# Model the real world with classes and objects (instances)
​
# Imperative programming - Writing code that runs one line at a time.
# Functional programming - Function-centric 
​
​
# Class - A description of a "kind" of thing. A Blueprint. A Template. 
# An Idea of a thing
​
# We define two types of things with classes
# State (Data) - (Attributes) (Properties)
# Behavior (Functions) - (Methods)
​
# Naming conventions for classes
# Capitalized CamelCase
# They should be singular (mostly)
​
# A blueprint for making a rabbit
# A class is a noun
class Rabbit:
    # Static State
    ears = 2
    # Dynamic State
    # The Initializer (Constructor)
    def __init__(self, name, color, age):
        # Attributes are adjective, or nouns
        # Setting a name attribute on the instance, to the name variable passed in
        self.name = name
        self.color = color
        self.age = age
​
    # Behavior - Instance Method - Always takes self as the first parameter
    # A Method is a verb
    def hop(self):
        # What is self?
        # It is the current instance
        print(self.name + " hops along.")
​
    def eat(self, amount):
        pass
​
    def run(self, x, y):
        pass
​
​
    
​
​
# Make actual rabbits (instances of the Rabbit class)
# What happens when you create an instance?
# 1. Python creates a new object
# 2. Python calls __init__ and passes it the new object as self
# 3. Python returns the new object
thumper = Rabbit("Thumper", "grey", 2)
flopsy = Rabbit("Flopsy", "white", 1)
​
thumper.run(100, 100)
thumper.eat(10)
​
print(type(thumper))
​
print(thumper.name)
print(thumper.age)
print(thumper.color)
print(thumper.ears)
​
thumper.hop()
flopsy.hop()
​
# print(rabbit.ears)
# print(thumper.ears)
# print(flopsy.ears)