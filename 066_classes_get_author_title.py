class Book:                                   
    # method initializer method with required state
    # parameters author and title
    def __init__(self, author, title):        
        # set self.author = author
        self.author = author                
        # set self.title = title
        self.title = title                  

    # method get_author(self)
    def get_author(self):                    
        # returns "Author: " + self.author
        return "Author: " + self.author      

    # method get_title(self)
    def get_title(self):                    
        # returns "Title: " + self.title
        return "Title: " + self.title         
#
print(book.get_author())
# prints "Author: Natalie Zina Walschots"
#
print(book.get_title())
# prints "Title: Hench"
