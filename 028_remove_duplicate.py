# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    result = []
    for let in s:
        if let not in result:
            result.append(let)
#    result = "".join(result)
    return result
        
#print(remove_duplicate_letters("abc"))
#print(remove_duplicate_letters("abcabc"))
#print(remove_duplicate_letters("abccba"))
#print(remove_duplicate_letters("abccbad"))
print(remove_duplicate_letters([1,2,1]))