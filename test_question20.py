def format_name(name):
    if name["middle"] == '':
        return f'{name["first"]} {name["last"]}'
    else:
        return f'{name["first"]} {name["middle"]} {name["last"]}'
    
print(format_name({"first": "Jocelyn", "middle": "", "last": "Payne"}
))
print(format_name({"first": "Jocelyn", "middle": "Maria", "last": "Payne"}
))