def sum_fraction_sequence(n):           # solution
    sum = 0                             # solution
    for i in range(1, n + 1):           # solution
        sum += i / (i + 1)              # solution
    return sum 
print(sum_fraction_sequence(1))
print(sum_fraction_sequence(2))
print(sum_fraction_sequence(3))