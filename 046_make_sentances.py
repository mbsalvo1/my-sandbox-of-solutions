# Examples:
#   * subjects: ["I"]
#     verbs:    ["play"]
#     objects:  ["Portal"]
#     returns:  ["I play Portal"]
#   * subjects: ["I", "You"]
#     verbs:    ["play"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "You play Portal", "You play Sable"]
#   * subjects: ["I", "You"]
#     verbs:    ["play", "watch"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "I watch Portal", "I watch Sable",
#                "You play Portal", "You play Sable"
#                "You watch Portal", "You watch Sable"]


def make_sentences(subjects, verbs, objects):
    list1 = []
    for a in (subjects):
        for b in (verbs):
            for c in (objects):
                complete = f'({a} hate to {b} {c}'
                list1.append(complete)
    return list1

print(make_sentences(["I", "You"], ["play", "watch"], ["Portal", "Sable"]))