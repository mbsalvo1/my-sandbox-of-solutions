def calc(left, op, right):
    if op == "+":
        return left + right
    if op == "-":
        return left - right
    if op == "*":
        return left * right
    if op == "/" and right > 0:
        return left / right
    else:
        return "cant divide by zero"

print(calc(2, "+" , 2))
print(calc(2, "/" , 0))
print(calc(2, "+" , 2))