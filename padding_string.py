def pad_left(number, length, pad):
    num = str(number)
    num2 = len(num)
    padding = pad * (length - num2)
    return padding + num
    
    

print(pad_left(10, 4, "*"))
print(pad_left(10, 5, "0"))
print(pad_left(1000, 3, "0"))
print(pad_left(19, 5, " "))