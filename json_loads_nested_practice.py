import json

sampleJson = """{ 
   "company":{ 
      "employee":{ 
         "name":"emma",
         "payble":{ 
            "salary":7000,
            "bonus":800
         }
      }
   }
}"""

data = json.loads(sampleJson)
print(data)
print(data['company'])
print(data['company']['employee'])
print(data['company']['employee']['payble'])
print(data['company']['employee']['payble']['salary'])