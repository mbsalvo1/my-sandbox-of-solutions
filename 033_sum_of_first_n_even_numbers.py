"""def sum_of_first_n_even_numbers(n):
    if n < 0:
        return None
    list1 = []
    if n == 0:
        return 0
    for number in range(1, 100):
        if number % 2 == 0:
            list1.append(number)
        if len(list1) == 5:
            return sum(list1)
print(sum_of_first_n_even_numbers(2))"""

"""print(list(range(0, 30, 5)))"""

# start of loop takes input number of how many in range
# + 1 . then in loop turning user input into even number
def sum_of_first_n_even_numbers(n): 
    if n < 0:
        return None
    total = 0
    for num in range(0, n + 1):
        total = total + num * 2
    return total
print(sum_of_first_n_even_numbers(500))