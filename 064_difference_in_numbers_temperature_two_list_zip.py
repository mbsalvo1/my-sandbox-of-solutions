def temperature_differences(highs, lows):     
    differences = []                          
    for high, low in zip(highs, lows):          
        differences.append(high - low)         
    return differences

print(temperature_differences([105, 110, 100], [70,88,89]))
