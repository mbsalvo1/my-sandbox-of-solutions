def find_shortest(lists):
    if lists == []:
        return "None"
    lists.sort(key=len)
    return lists[0]

print(find_shortest([[1,2,3], [3,2], [1,2,11,200]]))
print(find_shortest([[1,2,3],[1]]))
print(find_shortest([]))