
#2nd try

def sum_of_first_n_numbers(limit):
    if limit < 0:
        return None
    else:
        return sum(range(0, limit + 1))
        

# 1st try
"""def sum_of_first_n_numbers(limit):
    if limit < 0:
        return None    
    total = 0
    for num in range(0, limit + 1):
        total = num + total
    return total"""

print(sum_of_first_n_numbers(-1))
print(sum_of_first_n_numbers(0))
print(sum_of_first_n_numbers(1))
print(sum_of_first_n_numbers(2))
print(sum_of_first_n_numbers(5))