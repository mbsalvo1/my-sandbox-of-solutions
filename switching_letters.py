def switcheroo(s):
    letters = ""
    for i in s:
        if i == "a":
            letters += "b"
        elif i == "b":
            letters += "a"
        else:
            letters += i
    return letters

print(switcheroo("acb"))