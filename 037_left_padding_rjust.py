def pad_left(number, length, pad):
   
    return str(number).rjust(length, pad)

print(pad_left(10, 4, "*"))
print(pad_left(10, 5, "0"))
print(pad_left(1000, 3, "0"))
print(pad_left(19, 5, " "))

# switch r.just with l.just to move number left or right of padding