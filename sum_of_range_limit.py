def sum_of_first_n_numbers(limit):
    sum = 0
    if limit < 0:
        return "None"
    if limit == 0:
        return 0
    list = range(limit)
    for i in range(limit + 1):
        sum = sum + i
    return sum
        
        
print(sum_of_first_n_numbers(-1))
print(sum_of_first_n_numbers(0))
print(sum_of_first_n_numbers(1))
print(sum_of_first_n_numbers(2))
print(sum_of_first_n_numbers(5))
