"""def halve_the_list(input):                           
    return (                                           
        input[0:len(input) // 2 + (len(input) % 2)],    
        input[len(input) // 2 + (len(input) % 2):],    
    )
"""
    
                                                        
                                                  
                                                        
def halve_the_list(input):                             
    first_list = []                                     
    second_list = []                                    
    first_list_len = len(input) // 2 + (len(input) % 2)
    for i in range(first_list_len):                     
        first_list.append(input[i])                     
    for i in range(len(input) // 2):                    
        index = i + first_list_len                     
        second_list.append(input[index])                
    return first_list, second_list

print(halve_the_list(["just", "be", "do", "dont", "day"]))
print(halve_the_list([1,2,3,4,5,6,7]))
