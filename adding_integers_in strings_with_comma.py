def add_csv_lines(csv_lines):
    result_list = []                             
    for item in csv_lines:                           
        pieces = item.split(",")                     
        line_sum = 0                                
        for piece in pieces:                         
            value = int(piece)                        
            line_sum += value                        
        result_list.append(line_sum)                    
    return result_list

    
        
    

print(add_csv_lines([]))
print(add_csv_lines(["3", "1,9"]))
print(add_csv_lines(["8,1,7", "10,10,10", "1,2,3"]))