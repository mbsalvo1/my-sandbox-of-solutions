def findNeedle(haystack):
    if "needle" in haystack:
        return "found the needle at position " + str(haystack.index("needle"))




print(findNeedle(["hay", "toys","hay", "junk", "moreJunk", "junk", "hay", "junk","hay", "junk", "moreJunk","needle", "junk"]))
print(findNeedle(["hay", "junk","hay", "moreJunk","needle", "junk"]))
print(findNeedle(["hay", "needle","hay", "junk", "moreJunk", "junk"]))
print(findNeedle(["hay", "toys","hay", "junk", "moreJunk", "junk"]))