# base super class when others inherit from
class Animal:                                              
    def __init__(self, number_of_legs, primary_color):      
        self.number_of_legs = number_of_legs                
        self.primary_color = primary_color                 
# state number of legs and color
# self is instanace of the obeject
# method because its a function thats part of a class

    def describe(self):                                     
        return (                                         
            self.__class__.__name__                        
            + " has "                                       
            + str(self.number_of_legs)                      
            + " legs and is primarily "                    
            + self.primary_color                          
        )                                                 
                                            
# dog inherits from animal
# make a behavior of speak with a function
class Dog(Animal):                                        
    def speak(self):                                       
        return "Bark!"                                   
                                                            
                                                    
class Cat(Animal):                                         
    def speak(self):                                       
        return "Miao!"                                   
                                                            
                                                            
class Snake(Animal):                                        
    def speak(self):                                       
        return "Sssssss!"                                   

