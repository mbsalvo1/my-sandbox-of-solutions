def check_password(password):
    has_lowercase_letter = False                                        # solution
    has_uppercase_letter = False                                        # solution
    has_digit = False                                                   # solution
    has_special_char = False                                            # solution
    for character in password:                                          # solution
        if character.isalpha():                                         # solution
            if character.isupper():                                     # solution
                has_uppercase_letter = True                             # solution
            else:                                                       # solution
                has_lowercase_letter = True                             # solution
        elif character.isdigit():                                       # solution
            has_digit = True                                            # solution
        elif character == "$" or character == "!" or character == "@":  # solution
            has_special_char = True                                     # solution
    return (                                                            # solution
        len(password) >= 6                                              # solution
        and len(password) <= 12                                         # solution
        and has_lowercase_letter                                        # solution
        and has_uppercase_letter                                        # solution
        and has_digit                                                 
        and has_special_char                                            
    )
print(check_password("aP1!"))
