def gear_for_day(is_workday, is_sunny):
    gear = []
    if not is_sunny and is_workday:
        gear.append("umbrella")
    if is_workday:
        gear.append("laptop")
    if not is_workday:
        gear.append("surfboard")
    return gear
print(gear_for_day(True, False))
print(gear_for_day(True, True))
print(gear_for_day(False, True))
