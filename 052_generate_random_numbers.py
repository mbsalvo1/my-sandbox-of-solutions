import random                                   
                                                
"""def generate_lottery_numbers():                 
    numbers = list(range(1, 41))               
    random.shuffle(numbers)                     
    return numbers[0:6]"""




def generate_lottery_numbers():                 
    numbers = []                                
    while len(numbers) < 6:                     
        number = random.randint(1, 40)          
        if number not in numbers:               
            numbers.append(number)              
    return numbers

print(generate_lottery_numbers())
