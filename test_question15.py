def count_entries(line, separator):
    if line == "":
        return 0
    elif separator == "'":
        return "'"
    else:    
        return len(line.split(separator))
    
print(count_entries("",","))
print(count_entries("a",","))
print(count_entries(",",","))
print(count_entries("a,b,c",","))
print(count_entries("a,b,c","'"))