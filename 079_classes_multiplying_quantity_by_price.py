
class ReceiptItem:                             
    def __init__(self, quantity, price):        
        self.quantity = quantity                
        self.price = price                      

    def get_total(self):                        
        return self.quantity * self.price


item = ReceiptItem(10, 3.45)

print(item.get_total())    # Prints 34.5
