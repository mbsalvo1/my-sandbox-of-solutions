# def sum_evens(numbers):
#     return sum([i for i in numbers if i % 2 == 0])
#     
# 
# print(sum_evens([1,2,3,4]))
def merge_dictionaries(d1, d2):
    d3 = d1.copy()
    for key, value in d2.items():
        d3[key] = value
    return d3

print(merge_dictionaries({"a":1, "b":2}, {"b":"bbb", "c": "ccc"}))