# 
# def only_the_red(cars):
#     results = []
#     for i in cars:
#         for k, v in i.items():
#             if v == "red":
#                 results.append(i)
#     return results

def old_guzzlers(cars):
    results = []
    for i in cars:
        if i["year"] < 1980 and i["mpg"] < 12:
            results.append(i)
    
    return results

print(old_guzzlers([
    {"type": "truck", "color": "rusted", "year": 1955},
    {"type": "sedan", "color": "red", "year": 2015},
    {"type": "wagon", "color": "pea green", "year": 1965},
    {"type": "truck", "color": "blue", "year": 1995},
]))