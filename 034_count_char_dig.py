def count_letters_and_digits(s):
    char = 0
    num = 0
    for item in (s):
        if item.isdigit():
            num = num + 1
        if item.isalpha():
            char = char + 1
    return (char), (num)

print(count_letters_and_digits(""))
print(count_letters_and_digits("a"))
print(count_letters_and_digits("1"))
print(count_letters_and_digits("1a"))
print(count_letters_and_digits("1ab48"))