def check_age(name, age):
    if age < 21:
        return (f'Go home, {name}!')
    if age >= 21:
        return (f'Welcome, {name}!')


print(check_age("Adrian", 20))
print(check_age("Adrian", 21))
