def find_indexes(search_list, search_term):
    list = []
    for i , v in enumerate(search_list):
        if v == search_term:
            list.append(i)
    return list

print(find_indexes([1, 2, 3, 4, 5], 4))
print(find_indexes([1, 2, 3, 4, 5], 6))
print(find_indexes([1, 2, 1, 2, 1], 1))