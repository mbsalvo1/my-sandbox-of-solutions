class Person:
    def __init__(self, name, hated_foods, loved_foods):
        self.name = name
        self.hated_foods = hated_foods
        self.loved_foods = loved_foods
        
    def taste(self, food):
        if food in self.loved_foods:
            return True
        if food in self.hated_foods:
            return False
        if food in self.hated_foods or self.loved_foods:
            return None

person = Person("Malik",["cottage cheese", "sauerkraut"],
                    ["pizza", "schnitzel"])

print(person.taste("lasagna"))     # Prints None, not in either list
print(person.taste("sauerkraut"))  # Prints False, in the hated list
print(person.taste("pizza"))