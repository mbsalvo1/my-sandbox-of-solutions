def only_odds(nums):                # solution
    output = []                     # solution
    for num in nums:                # solution
        if num % 2 == 1:            # solution
            output.append(num)      # solution
    return output

print(only_odds([1, 2, 3, 4]))