class Employee:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
    def get_fullname(self):
        return self.first_name + " " + self.last_name
    def get_email(self):
# note can also assign variable for first and last name = first_name = last_name        
        return self.first_name.lower() + "." + self.last_name.lower() + "@company.com"

employee = Employee("Duska", "Ruzicka")
#
print(employee.get_fullname())  # prints "Duska Ruzicka"
print(employee.get_email())