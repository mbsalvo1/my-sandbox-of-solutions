
# using reverse slice
"""def reversed_string(word):
    if word == letter[::-1]:
        return "Congrats " + letter + " is a palindrome"
    else:
        return "Sorry " + letter + " is not a palindrome"

print(reversed_string("racecar"))"""

# using reverse join
def is_palindrome(word):
        if word == ''.join(reversed(word)):
            return "Congrats " + word + " is a palindrome!"
        else:
            return "Sorry " + word + " is not a palindrome!"
print(is_palindrome("racecar"))