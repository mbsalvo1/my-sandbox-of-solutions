# Write a class that meets these requirements.
#
# Name:       BankAccount
#
# Required state:
#    * opening balance, the amount of money in the bank account
#
# Behavior:
#    * get_balance()      # Returns how much is in the bank account
#    * deposit(amount)    # Adds money to the current balance
#    * withdraw(amount)   # Reduces the current balance by amount
#
# Example:
#    account = BankAccount(100)
#
#    print(account.get_balance())  # prints 100
#    account.withdraw(50)
#    print(account.get_balance())  # prints 50
#    account.deposit(120)
#    print(account.get_balance())  # prints 170
#
​
# Super Class (Base Class) (Parent Class)
class BankAccount():
    def __init__(self, initial_balance):
        self.balance = initial_balance
​
    def get_balance(self):
        print("get_balance in BankAccount")
        return self.balance
​
    def deposit(self, amount):
        print("deposit in BankAccount")
        self.balance += amount
​
    def withdraw(self, amount):
        print("withdraw in BankAccount")
        self.balance -= amount
​
# account = BankAccount(100)
​
# print(account.get_balance())  # prints 100
# account.withdraw(50)
# print(account.get_balance())  # prints 50
# account.deposit(120)
# print(account.get_balance())  # prints 170