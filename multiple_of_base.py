def is_multiple_of(number, base):
# != not equal to 
    if int(number) != number or int(base) != base:
        return False
    return number % base == 0
print(is_multiple_of(6, 2))
print(is_multiple_of(1, 2))
print(is_multiple_of(8, 1))