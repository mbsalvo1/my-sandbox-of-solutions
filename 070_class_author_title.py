class Book:
    def __init__(self, author_name, title):
        self.author_name = author_name
        self.title = title
    
    def get_author(self):
        return ("Author: " + self.author_name)
                    
    def get_title(self):
        return ("Title: " + self.title)
book = Book("Natalie Zina Walschots", "Hench")

print(book.get_author())  # prints "Author: Natalie Zina Walschots"
print(book.get_title())   # prints "Title: Hench"