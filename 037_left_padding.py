def pad_left(number, length, pad):
    number = str(number)
    number2 = len(number)
    if length <= len(number):
        length = 0
    length = pad * (length-number2)
    return length + number
print(pad_left(10, 4, "*"))
print(pad_left(10, 5, "0"))
print(pad_left(1000, 3, "0"))
print(pad_left(19, 5, " "))

# switch return length and number for left and right padding